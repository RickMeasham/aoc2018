#!/usr/bin/py

from collections import Counter

with open('Day2.input.txt') as f:
    content = f.readlines()

content = [x.strip() for x in content]

count_has_2 = 0
count_has_3 = 0

for one_line in content:
    counts = Counter(list(one_line))

    if len(list(filter(lambda d: counts[d] == 2, counts.keys()))):
        count_has_2 += 1

    if len(list(filter(lambda d: counts[d] == 3, counts.keys()))):
        count_has_3 += 1

total = count_has_2 * count_has_3

print(total)
