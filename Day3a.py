#!/usr/bin/py

# re to parse the lines in the file
import re

with open('Day3.input.txt') as f:
    content = f.readlines()
content = [x.strip() for x in content]


# Tried to get numpy to work, but in the end old fashioned arrays of arrays of arrays won
def AoAoA(w, h):
    aoaooa = []
    for x in range(w):
        aoaooa.append([])
        for y in range(h):
            aoaooa[x].append([])
    return aoaooa


# Create an array-of-arrays containing empty arrays that's 1000 x 1000
claims = AoAoA(1000, 1000)

# Store all the data in the AoAoA. Note that we're storing the claim
# number in an array as I suspect we're abuot to need it in part two
for line in content:
    matched_line = re.match(r'#(?P<num>\d+) @ (?P<x>\d+),(?P<y>\d+): (?P<w>\d+)x(?P<h>\d+)', line)
    claim = matched_line.groupdict()

    for y_index in range(int(claim['y']), int(claim['y']) + int(claim['h'])):
        for x_index in range(int(claim['x']), int(claim['x']) + int(claim['w'])):
            claims[x_index][y_index].append(claim['num'])

# Nested loop to count squares with multiple claims. There's probably a shorter way to do this.
counter = 0
for x_index in range(len(claims)):
    for y_index in range(len(claims[x_index])):
        if len(claims[x_index][y_index]) > 1:
            counter += 1

print(counter)
