# /usr/bin/py

with open('Day1a.input.txt') as f:
    content = f.readlines()

content = [x.strip() for x in content]

total = 0

for one_line in content:
    direction = one_line[0]
    number = int(one_line[1:])

    if direction == '-':
        total = total - number
    else:
        total = total + number

print(f'Total: {total}')
