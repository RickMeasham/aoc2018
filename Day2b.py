#!/usr/bin/py

import Levenshtein, difflib

with open('Day2.input.txt') as f:
    content = f.readlines()

content = [x.strip() for x in content]

# Somewhere there's a line with a levenshtein distance of '1'. Only one line so we can stop when we find it.

for x in range(0, len(content)):
    for y in range(x + 1, len(content)):
        if Levenshtein.distance(content[x], content[y]) == 1:
            print(f'{content[x]} and {content[y]} are different by one letter')
            print(''.join(difflib.ndiff(content[x], content[y])))
            exit()
