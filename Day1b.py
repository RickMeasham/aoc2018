# /usr/bin/py

with open('Day1a.input.txt') as f:
    content = f.readlines()

content = [x.strip() for x in content]

total = 0
loop_count = 0
everything_we_have_ever_seen = []

while True:
    loop_count += 1

    for one_line in content:
        direction = one_line[0]
        number = int(one_line[1:])

        if direction == '-':
            total = total - number
        else:
            total = total + number

        if everything_we_have_ever_seen.count(total) > 0:
            print(f'Found {total} in the list already. Our work here is done after {loop_count} loops.')
            exit()

        everything_we_have_ever_seen.append(total)
